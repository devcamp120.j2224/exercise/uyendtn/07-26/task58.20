package com.devcamp.s50.task5820.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5820.restapi.model.CDrinks;

public interface IDrinks extends JpaRepository<CDrinks,Long> {
    
}
